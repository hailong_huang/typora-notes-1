# SpringMVC

MVC: 模型（dao，service）  视图（jsp） 控制器（Servlet）

## 1.父工程导入依赖

```xml
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId> 
    <version>5.2.0.RELEASE</version>
</dependency>
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>servlet-api</artifactId>
    <version>2.5</version>
</dependency>
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>jstl</artifactId>
    <version>1.2</version>
</dependency>
<dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.1</version>
        </dependency>
```

## 2.新建工程

#### 1.新建moudle，添加web支持

#### 2.确定导入了springmvc依赖

#### 3.配置web.xml，注册DispatcherServlet

```
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <!--配置DispatcherServlet，请求分发器，前端控制器-->
    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--DispatcherServlet要绑定Spring的配置文件-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc-servlet.xml</param-value>
        </init-param>
        <!--启动级别:1  和服务器一起启动-->
        <load-on-startup>1</load-on-startup>

    </servlet>


    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
</web-app>
```

#### 4.编写springMVC的配置文件（springmvc-servlet.xml）

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">
        
    <!--处理器映射器-->
    <bean class="org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping"/>

    <!--处理器适配器-->
    <bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter"/>

    <!--视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/jsp/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
    
</beans>
```

#### 5.编写业务controller,返回ModelAndView，装数据，封视图

```
public class HelloController implements Controller {
    @Override
    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

        ModelAndView mv = new ModelAndView();

        //业务逻辑
        String result = "helloSpringMvc";

        mv.addObject("msg",result);

        //视图跳转

        mv.setViewName("test");

        return mv;
    }
}
```

并在springmvc-servlet.xml中注册bean

```
<!--BeanNameUrlHandlerMapping:bean-->
<bean id="/hello" class="com.gzk.controller.HelloController"/>
```

## 3.注解开发

####  1.配置web.xml

```
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <!--配置DispatcherServlet，请求分发器，前端控制器-->
    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--DispatcherServlet要绑定Spring的配置文件-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc-servlet.xml</param-value>
        </init-param>
        <!--启动级别:1  和服务器一起启动-->
        <load-on-startup>1</load-on-startup>

    </servlet>


    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
</web-app>
```

#### 2.配置resources包下springmvc-servlet.xml

 为了支持基于注解的IOC，设置了自动扫描包的功能

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        https://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--自动扫描包，让指定包下的注解生效，由IOC容器统一管理-->
    <context:component-scan base-package="com.gzk.controller"/>
    <!--让springmvc不处理静态资源 .css .js .mp3 .html-->
    <mvc:default-servlet-handler/>
    <!--
    支持mvc注解驱动
    在spring中一般采用@RequestMapping注解来完成映射关系
    想要使其生效
    必须向上下文中注册DefaultAnnotationHandleMapping
    和一个AnnotationMethodHandlerAdapter实例
    这两个实例分别在累级别和方法级别处理
    而annotation-drive配置帮助我们自动完成上述两个实例的注入
    -->
    <mvc:annotation-driven/>


    <!--视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/jsp/"/>
        <property name="suffix" value=".jsp"/>
    </bean>
    
</beans>
```

#### 3.编写controller

```
@Controller
public class HelloController {


    @RequestMapping("/hello")
    public String hello(Model model) {
        //封装数据
        model.addAttribute("msg","Hello,SpringMVCAnnotation");
        return "hello"; //会被视图解析器处理
    }

    @RequestMapping("/hello2")
    public String hello2(Model model) {
        //封装数据
        model.addAttribute("msg","Hello,SpringMVCAnnotation");
        return "hello"; //会被视图解析器处理
    }

    @RequestMapping("/hello3")
    public String hello3(Model model) {
        //封装数据
        model.addAttribute("msg","Hello,SpringMVCAnnotation");
        return "hello"; //会被视图解析器处理
    }
}
```

#### 4.@RequestMapping注解

```
@RequestMapping("/add/{a}/{b}")
public String hello2(@PathVariable int a, @PathVariable int b, Model model) {
    int result = a + b;
    model.addAttribute("msg","Hello2:" + result);
    return "hello"; //会被视图解析器处理
}
```

#### 5.重定向

```
public String test(Model model) {
    model.addAttribute("msg","ModelTest1");
    return "redirect:/index.jsp"; 
}
```

#### 6.@RequestParam注解

```
@GetMapping("/t1")
public String test1(@RequestParam("username") String name, Model model) {

    model.addAttribute("msg",name);

    return "hello";
}
```

#### 7.乱码过滤

在web.xml中添加

```
<!--springmvc的乱码过滤-->
<filter>
    <filter-name>encoding</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
        <param-name>encoding</param-name>
        <param-value>utf-8</param-value>
    </init-param>
</filter>
<filter-mapping>
    <filter-name>encoding</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
```

## 4.JSON

json是一种轻量级的数据交换格式

采用完全独立于编程语言的文本格式来储存和表示数据

#### 1.JSON与js的转换

```
//将js对象转化为json对象
var json = JSON.stringify(user);
console.log(json)

//将json对象转化为js对象
var obj = JSON.parse(json);
console.log(obj)
```

#### 2.使用JSON

###### 1.导入依赖jackson

```
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.12.1</version>
</dependency>
```

###### 2.建立实体类

```
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String name;
    private int age;
    private String sex;
}
```

可以导入lombok依赖，增加开发效率

```
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.10</version>
</dependency>
```

###### 3.建立controller

```
@Controller
public class UserController {

    @RequestMapping("/j1")
    @ResponseBody//他不会走视图解析器,会直接返回一个字符串
    public String json1() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        User user = new User("郭泽凯1",19,"男");

        String s = mapper.writeValueAsString(user);

        return s;
    }
}
```

###### 4.springMVC解决乱码问题

springmvc配置文件中添加

```
<!--JSON乱码问题配置-->
<mvc:annotation-driven>
    <mvc:message-converters register-defaults="true">
        <bean class="org.springframework.http.converter.StringHttpMessageConverter">
            <constructor-arg value="UTF-8"/>
        </bean>
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property name="objectMapper">
                <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                    <property name="failOnEmptyBeans" value="false"/>
                </bean>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

###### 5.date

纯java

```
@RequestMapping("/j2")
@ResponseBody//他不会走视图解析器,会直接返回一个字符串
public String json2() throws JsonProcessingException {

	ObjectMapper mapper = new ObjectMapper();
    Date date = new Date();
    //自定义日期格式
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    String s = mapper.writeValueAsString(dateFormat.format(date));

    return s;
}
```

ObjectMapper来格式化输出

```
@RequestMapping("/j3")
@ResponseBody//他不会走视图解析器,会直接返回一个字符串
public String json3() throws JsonProcessingException {

    ObjectMapper mapper = new ObjectMapper();
    //关闭时间戳方式
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
    //自定义日期格式
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    mapper.setDateFormat(dateFormat);

    Date date = new Date();

    String s = mapper.writeValueAsString(dateFormat.format(date));

    return s;
}
```

###### 6.抽取工具类JsonUtils

```
public class JsonUtils {

    public static String getJson(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }


    public static String getJson(Object object,String dateFormat) {
        ObjectMapper mapper = new ObjectMapper();
        //关闭时间戳方式
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        //自定义日期格式
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        mapper.setDateFormat(df);

        try {
            return mapper.writeValueAsString(object);
        }catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

#### 3.fastjson

###### 1.导入依赖

```
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.60</version>
</dependency>
```

###### 2.test

```
@RequestMapping("/j4")
@ResponseBody//他不会走视图解析器,会直接返回一个字符串
public String json4() throws JsonProcessingException {
    List<User> lists = new ArrayList<>();
    User user1 = new User("郭泽凯1",18,"nan");
    User user2 = new User("郭泽凯2",18,"nan");
    User user3 = new User("郭泽凯3",18,"nan");
    User user4 = new User("郭泽凯4",18,"nan");
    lists.add(user1);
    lists.add(user2);
    lists.add(user3);
    lists.add(user4);

    return JSON.toJSONString(lists);
}
```

